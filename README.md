# Reproduction de la vulnérabilité Spring4Shell (CVE-2022-22965)

Il s'agit d'une application dockerisée qui est vulnérable à la vulnérabilité Spring4Shell. L'application, c'est juste une page hello world pour explorer la vulnérabilité en question.

## Résumé

La vulnérabilité "Spring4Shell", est une faille de sécurité spécifique identifiée et répertoriée dans la base des CVE (Common Vulnerabilities and Exposures) avec cet identifiant unique.
Le CVE-2022-22965 fait référence à la vulnérabilité précise au sein du framework Spring Framework version 4.x, qui permet à un attaquant d'exécuter du code malveillant à distance en exploitant une mauvaise gestion des entrées utilisateur.

## Présentation

La vulnérabilité "Spring4Shell" est une faille de sécurité qui affecte les applications utilisant le framework Spring Framework version 4.x. Elle est également connue sous le nom de "Remote Code Execution" (RCE).Elle permettrait à un attaquant de manipuler les entrées utilisateur pour exécuter du code malveillant à distance sur le serveur. En exploitant cette faille, un attaquant peut exécuter des commandes système arbitraires sur le serveur où l'application vulnérable est déployée.

L'exploitation de cette vulnérabilité peut entraîner la compromission du serveur, l'accès à des informations sensibles, la modification de données ou d'autres actions malveillantes. 

## Présentation du projet Spring Boot dockerisé

### Mise en place de l'environnement

1. Installation de Docker.
2. Installation de Python 3 et ajout de quelques bibliothèques.

### Mise en place du projet

1. Clonage du projet.
![WebPage](screenshots/1.png?raw=true)
2. Construction de l'image et du conteneur 
![WebPage](screenshots/2.png?raw=true)

La commande suivante construit une image Docker à partir des fichiers présents dans le répertoire actuel, lui donne le tag "spring4shell", puis exécute un conteneur à partir de cette image en mappant le port 8080 de l'hôte sur le port 8080 du conteneur. Cela permet de démarrer un service accessible depuis l'hôte sur le port 8080.

![WebPage](screenshots/3.png?raw=true)

Spring à bien demarrer

![WebPage](screenshots/4.png?raw=true)

Notre application est bien déployée, et nous voyons la page de test, comme illustré sur l'image ci-dessus

## Exploitation de la vulnérabilité : 

1. Exploitation du fichier python (l'exploit)

![WebPage](screenshots/5.png?raw=true)

2. Exécution du script exploit.py

![WebPage](screenshots/6.png?raw=true)

L’exécution du script permet de créer un shell : shell.jsp qui sera utilisé par l’attaquant pour l'exécution de commande à distance, d'où la faille de type RCE.

Comme on peut le voir sur la capture précédente, l’encadré en rouge représente le Payload. On pourra modifier la valeur du paramètre GET : CMD pour l’envoyer des commandes.
Nous effectuons donc une démonstration avec quelques commandes usuelles, sur ma machine virtuelle linux mise en place dans le cadre du TP :

 - Commande : cmd = id, 
	La commande "id" affiche de manière concise les informations d'identification de l'utilisateur actuel sous Linux, notamment son UID, son GID, les groupes auxquels il appartient et son nom d'utilisateur. C'est utile pour vérifier rapidement les informations d'identification d'un utilisateur dans un environnement Linux. 
Comme le montre la capture ci-dessous dans notre cas.

![WebPage](screenshots/7.png?raw=true)

- Commande : cmd = ls
La commande "ls" est une commande couramment utilisée dans les systèmes d'exploitation de type Linux. Elle permet de lister les fichiers et les répertoires présents dans un répertoire donné. Comme le montre la capture ci-dessous dans notre cas 

![WebPage](screenshots/8.png?raw=true)

- Commande : cmd = ls -l
La commande "ls -l" est une variation de la commande "ls" utilisée dans les systèmes d'exploitation de type Linux. Elle permet d'afficher les fichiers et les répertoires avec des détails supplémentaires, tels que les autorisations, les propriétaires, les tailles, les dates de modification, etc. Comme le montre la capture ci-dessous dans notre cas 

![WebPage](screenshots/9.png?raw=true)

- Commande : cmd = uname -a
La commande "uname -a" permet d'obtenir rapidement des informations importantes sur le système d'exploitation, le noyau et la configuration matérielle. . Comme le montre la capture ci-dessous dans notre cas 

![WebPage](screenshots/10.png?raw=true)

## Conclusion
 la reproduction du CVE-2022-22965 implique la configuration d'une application dockerisée vulnérable, suivie de l'exploitation de la vulnérabilité à l'aide du script "exploit.py".
Il est important de noter que la vulnérabilité n'est présente que dans les applications utilisant le framework Spring Framework version 4.x. Les versions ultérieures de Spring Framework peuvent contenir des correctifs qui résolvent cette vulnérabilité. Il est donc recommandé de mettre à jour les applications vers des versions corrigées du framework afin de prévenir toute exploitation de cette vulnérabilité.

## Sources :
https://nvd.nist.gov/vuln/detail/cve-2022-22965
https://www.cisa.gov/known-exploited-vulnerabilities-catalog
https://github.com/reznok/Spring4Shell-POC